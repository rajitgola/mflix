import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { IInterfaceState } from '../../shared/model';

const initialState: IInterfaceState = {
    showSidebar : false,
    scrollViewMovieList : 0
};

export const interfaceSlice = createSlice({
    name: 'interface',
    initialState,
    reducers: {
        updateScrollView: (state, action: PayloadAction<number>) => {
            state.scrollViewMovieList += action.payload;
        },
        updateSidebarState : (state, action: PayloadAction<boolean>) => {
            state.showSidebar = action.payload;
        },
        resetScrollView : (state) => {
            state.scrollViewMovieList = 0;
        }
    }
  });
  
  export const { updateScrollView, updateSidebarState, resetScrollView } = interfaceSlice.actions;
  
  export const movieList = (state: RootState) => state.movies;
  
  export default interfaceSlice.reducer;