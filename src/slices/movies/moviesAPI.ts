import { MovieListData } from "../../constants"

export function fetchMovies() {
  return new Promise<{ data: any[] }>((resolve) => {
    // setTimeout(() => resolve(MovieListData), 500); 
    fetch(`https://dev-brinx.bloksports.com/admin/v1/get/interview-data`)
    .then(async (res) => {
      const response = await res.json();
      resolve(response.data)
    })
  })
}
  