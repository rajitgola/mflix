import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { IMovie, LoadStatusEnum, MoviesState } from '../../shared/model';
import { fetchMovies } from './moviesAPI';


const initialState: MoviesState = {
  data: [],
  filteredData: [],
  genre : "",
  status: LoadStatusEnum.idle,
  modal : { isOpen : false, movie : null }
};

export const fetchMovieAsync = createAsyncThunk(
    'movies/fetchMovies',
    async () => {
      const response = await fetchMovies();
      return response;
    }
  );
  
export const moviesSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        fetchStatus: (state, action: PayloadAction<LoadStatusEnum>) => {
          state.status = action.payload;
        },
        updateMovies : (state, action: PayloadAction<Array<IMovie>>) => {
          state.data = action.payload;
          state.filteredData = action.payload;
        },
        filterMovies : (state, action: PayloadAction<string>) => {
          state.genre = action.payload;
          state.filteredData = state.data.filter((x : IMovie) => x.genres.includes(action.payload));
        },
        clearFilter : (state) => {
          state.genre = "";
          state.filteredData = state.data;
        },
        updateModal : (state, action: PayloadAction<{ isOpen : boolean, movie : IMovie | null }>) => {
          state.modal.isOpen = action.payload.isOpen;
          state.modal.movie = action.payload.movie;
        },
    },
    extraReducers: (builder) => {
      builder
        .addCase(fetchMovieAsync.pending, (state) => {
          state.status = LoadStatusEnum.loading;
        })
        .addCase(fetchMovieAsync.rejected, (state) => {
          state.status = LoadStatusEnum.failed;
        })
        .addCase(fetchMovieAsync.fulfilled, (state, action) => {
          state.status = LoadStatusEnum.loaded;
          state.data = action.payload as any as Array<IMovie>;
          state.filteredData = action.payload as any as Array<IMovie>;
        });
    },
  });
  
  export const { fetchStatus, updateMovies, filterMovies, clearFilter, updateModal } = moviesSlice.actions;
  
  export const movieList = (state: RootState) => state.movies;
  
  export default moviesSlice.reducer;