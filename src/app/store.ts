import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import interfaceSlice from '../slices/interface/interfaceSlice';
import moviesSlice from '../slices/movies/moviesSlice';

export const store = configureStore({
  reducer: {
    movies : moviesSlice,
    interface : interfaceSlice
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
