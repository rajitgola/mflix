import React from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { IMovie } from '../../shared/model';
import { updateModal } from '../../slices/movies/moviesSlice';
import './Modal.scss';
const Modal = () => {
    const dispatch = useAppDispatch();
    const modalInfo : { isOpen : boolean, movie : IMovie | null } = useAppSelector((state) => state.movies.modal);
    const closeModal = () => {
        dispatch(updateModal({isOpen : false, movie : null}));
    }
    console.log(modalInfo.movie);
    return (
        <>
            {
                modalInfo.isOpen && 
                <div className={`modal-overlay ${modalInfo?.isOpen ? `isModalOpen` : null}`}>
                    <div className="modal-container">
                        <div className="row">
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                                <div className="modal__image">
                                    <img src={modalInfo.movie?.poster} alt={modalInfo?.movie?.title} />
                                </div>
                            </div>
                            <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                                <div className="modal__details text-start">
                                    <h4 className='text-primary mb-0'>{modalInfo?.movie?.title}</h4>
                                    { modalInfo?.movie && 
                                        <p className="mb-0 text-primary">
                                            {new Date(modalInfo?.movie?.released).getFullYear()} | {modalInfo.movie.runtime} Min
                                        </p>
                                    }
                                    <div className='my-3'>
                                        <h6>Plot</h6>
                                        <p className='mb-1'>
                                            {modalInfo?.movie?.plot}
                                        </p>
                                    </div>
                                    <hr className='my-1'></hr>
                                    <div className='my-3'>
                                        <h6>Directors</h6>
                                        <p className='mb-1'>
                                            {modalInfo?.movie?.directors}
                                        </p>
                                    </div>
                                    <hr className='my-1'></hr>
                                    <div className='my-3'>
                                        <h6>Actors</h6>
                                        <p className='mb-1'>
                                            {modalInfo?.movie?.cast}
                                        </p>
                                    </div>
                                    <hr className='my-1'></hr>
                                    <div className='my-3 justify-content-between d-flex'>
                                        <h6>Rating </h6>
                                        <span className="badge bg-success fs-6">{modalInfo?.movie?.imdb.rating}/10</span>
                                    </div>
                                    <hr className='my-1'></hr>
                                    <div className='my-3'>
                                        {modalInfo?.movie?.genres.map((genre, index) => {
                                            return (
                                                <span className='badge bg-primary fs-6 me-2' key={index}>
                                                    {genre}
                                                </span>
                                            )
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="modal__action pointer">
                            <a onClick={closeModal} className='fs-4 text-primary'>
                                <span className="far fa-times"></span>
                            </a>
                        </div>
                        {/* <button className="btn btn-primary" >
                            close
                        </button> */}
                    </div>
                </div>
            }
        </>
    )
}

export default Modal;
