import React from 'react';
import { useAppDispatch, useAppSelector } from '../../app/hooks';
import { flatten } from '../../shared/helper';
import { IMovie } from '../../shared/model';
import { resetScrollView, updateSidebarState } from '../../slices/interface/interfaceSlice';
import { clearFilter, filterMovies } from '../../slices/movies/moviesSlice';
import './Sidebar.scss';
const Sidebar = () => {

    const dispatch = useAppDispatch();
	const showSidebar : boolean = useAppSelector((state) => state.interface.showSidebar);
    const movies : Array<IMovie> = useAppSelector((state) => state.movies.data);
    
    const toggleSidebar = () => {
        dispatch(updateSidebarState(false));
    }

    const onMovieFilter = (genre : string) => {
        if(genre) {
            dispatch(resetScrollView())
            dispatch(filterMovies(genre));
        } else {
            dispatch(resetScrollView())
            dispatch(clearFilter())
        }
        dispatch(updateSidebarState(false));
    }

    const genres : Array<string> =  Array.from(new Set(flatten(movies.map(x=>x.genres)))); 

    return (
        <>
            <nav id="sidebar"
                className={
                    showSidebar ? 'active bg-dark' : 'bg-dark'
                }>
                <h1 className='text-start'>
                    <a className="logo text-primary">
                        <span className="text-primary">MFLIX</span> 
                        <span className='fs-6 text-white d-block'>Genres</span>
                    </a>
                </h1>
                <div className="sidebar--action pointer">
                    <a className='text-white fs-4' onClick={toggleSidebar}>
                        <span className="far fa-times"></span>
                    </a>
                </div>
                <ul className="list-styled text-start">
                    <li className='pointer'>
                        <a onClick={() => onMovieFilter("")}>
                            <span className="fas fa-camera pe-1"></span> All 
                        </a>
                    </li>
                    {
                        genres && genres.map((genre : string, index : number) => {
                                return (
                                    <li key={index} className='pointer' onClick={() => onMovieFilter(genre)}>
                                        <a><span className="fas fa-camera pe-1"></span> {genre} </a>
                                    </li>
                                )
                        })
                    }   
                </ul>
            </nav>
        </>
    )
}

export default Sidebar;
