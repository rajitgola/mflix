import React from 'react';
import { useAppDispatch } from '../../app/hooks';
import { IMovie } from '../../shared/model';
import { updateModal } from '../../slices/movies/moviesSlice';
import './MovieCard.scss';
const MovieCard = (movie : IMovie) => {
    const dispatch = useAppDispatch();
    const viewDetails = () => {
        dispatch(updateModal({isOpen : true, movie}));
    }

    return (
        <>
            <div className="col">
                <div className="card bg-black">
                    <div className="card--image pointer" onClick={viewDetails}
                        style={{ backgroundImage: 'url(' + movie.poster + ')' }}>
                        {/* <img src={movie.poster} className="card-img-top" alt="..."></img> */}
                    </div>
                    <div className="card-body text-start">
                        <h6 className="card-title mb-1 text-white">{movie.title}</h6>
                        <p className="card-text text-primary fw-bold mb-2">
                            {movie.languages}
                        </p>
                        {movie.imdb.rating && <span className="badge bg-success fs-6">{movie.imdb.rating}/10</span>}
                    </div>
                </div>
            </div>
        </>
    )
}

export default MovieCard;
