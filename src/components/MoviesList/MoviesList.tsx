import React, { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from "../../app/hooks";
import { IMovie, LoadStatusEnum } from '../../shared/model';
import { updateScrollView } from '../../slices/interface/interfaceSlice';
import { fetchMovieAsync } from '../../slices/movies/moviesSlice';
import MovieCard from "../MovieCard";
import './MoviesList.scss';
const MoviesList = () => {

	const dispatch = useAppDispatch();

	const movies : Array<IMovie> = useAppSelector((state) => state.movies.filteredData);
	const status : LoadStatusEnum = useAppSelector((state) => state.movies.status);
    const scrollViewSpace : number = useAppSelector((state) => state.interface.scrollViewMovieList);

    useEffect(() => {
        dispatch(fetchMovieAsync());
	}, []);

    const onTraverseLeft = () => {
        dispatch(updateScrollView(1000))
    }

    const onTraverseRight = () => {
        dispatch(updateScrollView(-1000))
    }

    const style = {
        transform: `translate3d(${scrollViewSpace}px, 0px, 0px)`
    }

    return (
        <>
			{
				status == LoadStatusEnum.loading && <div><h1>Loading...</h1></div>
			}
            {status == LoadStatusEnum.loaded && 
                <div className='movie-list-outer'>
                    <div className="row g-4 movie-list" style={style}>
                        {movies && movies.map((movie, index) => {
                            return <MovieCard key={index} {...movie} />
                        })}                
                    </div>
                    <div className="movie-list--action">
                        { scrollViewSpace < 0 &&
                            <a className="pointer text-decoration-none movie-list--action__left fs-3" onClick={onTraverseLeft}>
                                <span className="fa fa-chevron-left"></span>
                            </a>
                        }
                        <a className="pointer text-decoration-none movie-list--action__right fs-3" onClick={onTraverseRight}>
                            <span className="fa fa-chevron-right"></span>
                        </a>
                    </div>
                </div>
            }
        </>
    )
}

export default MoviesList;
