import React from 'react';
import { useAppDispatch } from '../../app/hooks';
import { updateSidebarState } from '../../slices/interface/interfaceSlice';

const Navbar = () => {

    const onBrandAction = () => {
        console.log("onBrandAction");
    }

    const onNotificationAction = () => {
        console.log("onNotificationAction");
    }

    const dispatch = useAppDispatch();
    
    const toggleSidebar = () => {
        dispatch(updateSidebarState(true));
    }
    
    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light bg-dark">
                <div className="container-fluid">
                    <a className="navbar-toggler d-block text-white border-0 pointer"
                         aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"
                         onClick={toggleSidebar}>
                        <span className="navbar-toggler-icon fa fa-bars"></span>
                    </a>
                    <a className="navbar-brand d-flex flex-column text-white" onClick={onBrandAction}>
                        <span className='fs-2 text-uppercase text-primary'>MFlix</span>
                        <span className='font-extra-small text-start'>by RAJIT</span>
                    </a>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                        <ul className="navbar-nav mb-2 mb-lg-0">
                            <li className="nav-item">
                                <a className="nav-link fs-4 text-white pointer" aria-current="page"
                                    onClick={onNotificationAction}>
                                    <span className="fa fa-bell"></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}

export default Navbar;

