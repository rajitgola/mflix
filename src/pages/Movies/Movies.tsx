import React from 'react';
import Modal from '../../components/Modal';
import MoviesList from "../../components/MoviesList";
import Navbar from "../../components/Navbar";
import Sidebar from '../../components/Sidebar';
import './Movies.scss';
const Movies = () => {
    return (
        <>
            <section className='bg-black movie-list-container'>
                <Navbar></Navbar>
                <Sidebar></Sidebar>
                <Modal></Modal>
                <div className="container-fluid">
                    <div className="my-5">
                        <h4 className="text-white text-start">New on MFlix</h4>
                        <div className="my-4">
                            <MoviesList></MoviesList>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default Movies;
