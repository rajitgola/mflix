export interface IInterfaceState {
    showSidebar : boolean,
    scrollViewMovieList : number
}

export interface Viewer {
    rating: number;
    numReviews: number;
    meter: number;
}

export interface Tomatoes {
    viewer: Viewer;
    dvd: Date;
    website: string;
    production: string;
    lastUpdated: Date;
}

export interface Award {
    wins: number;
    nominations: number;
    text: string;
}

export interface Imdb {
    rating: number;
    votes: number;
    id: number;
}

export interface IMovie {
    tomatoes: Tomatoes;
    genres: string[];
    cast: string[];
    languages: string[];
    directors: string[];
    countries: string[];
    _id: string;
    plot: string;
    runtime: number;
    num_mflix_comments: number;
    poster: string;
    title: string;
    lastupdated: string;
    released: Date;
    writers: string[];
    awards: Award[];
    year: number;
    imdb: Imdb;
    type: string;
    lasupdated: Date;
}

export interface MoviesState {
  data: Array<IMovie>;
  filteredData: Array<IMovie>;
  genre : string
  status: LoadStatusEnum;
  modal : { isOpen : boolean, movie : IMovie | null }
}

export enum LoadStatusEnum {
    idle = "idle",
    loading = "loading",
    failed = "failed",
    loaded = "loaded"
}