import React from 'react';
import { BrowserRouter as Router, Navigate, Route, Routes } from "react-router-dom";
import './App.scss';
import Movies from './pages/Movies';

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          {/* <Route exact path="/" element={<Movies />}> </Route> */}
          {/* <Redirect from="/" to="/movies" /> */}
          <Route path="/" element={<Navigate replace to="/movies" />} />
          <Route path="/movies" element={<Movies />}> </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
